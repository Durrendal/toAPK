#Alpine provides Lua 5.3 as Lua5.3, Lua is by default symlinked to Lua 5.1
#This can be changed to simply lua as needed/desired
LUA ?= lua5.3
LUAV = 5.3
LUA_SHARE=/usr/share/lua/$(LUAV)
#On Debian /usr/lib/lib/x86_64-linux-gnu/liblua5.3.a would be used, Alpine packages it a little differently, so we use the following
STATIC_LUA_LIB ?= /usr/lib/liblua-5.3.so.0.0.0
LUA_INCLUDE_DIR ?= /usr/include/lua5.3
LUA_ROCKS ?= /usr/bin/luarocks-5.3
DESTDIR=/usr/bin

fetch-libraries:
	#$(LUA_ROCKS) install luasocket --local
	$(LUA_ROCKS) install luasec --local

compile-lua:
	$(MAKE) fetch-libraries
	fennel --compile src/toAPK.fnl --add-macro-path src/macros.fnl > src/toAPK.lua
	sed -i '1 i\-- Author: Will Sinatra <wpsinatra@gmail.com> | License: GPLv3' src/toAPK.lua
	sed -i '1 i\#!/usr/bin/$(LUA)' src/toAPK.lua

install-lua:
	install ./src/toAPK.lua -D $(DESTDIR)/toAPK

compile-bin:
	cd ./src/ && fennel --compile-binary toAPK.fnl toAPK-bin $(STATIC_LUA_LIB) $(LUA_INCLUDE_DIR)

install-bin:
	install ./src/toAPK-bin -D $(DESTDIR)/toAPK

test-pkgbuild:
	./src/./toAPK-bin -a example/ARCH-Pkgbuilds/fennel > example/Converted-Apkbuilds/fennel
	./src/./toAPK-bin -a example/ARCH-Pkgbuilds/zapm > example/Converted-Apkbuilds/zapm
	./src/./toAPK-bin -a example/ARCH-Pkgbuilds/chez > example/Converted-Apkbuilds/chez
	./src/./toAPK-bin -a example/ARCH-Pkgbuilds/tensorflow > example/Converted-Apkbuilds/tensorflow
	./src/./toAPK-bin -a example/ARCH-Pkgbuilds/gcloud-sdk > example/Converted-Apkbuilds/gcloud-sdk
	./src/./toAPK-bin -a example/ARCH-Pkgbuilds/request-tracker > example/Converted-Apkbuilds/request-tracker

test-template:
	./src/./toAPK-bin -v example/VOID-Templates/angband > example/Converted-Apkbuilds/angband
	./src/./toAPK-bin -v example/VOID-Templates/kismet/template > example/Converted-Apkbuilds/kismet
	./src/./toAPK-bin -v example/VOID-Templates/librecad > example/Converted-Apkbuilds/librecad
	./src/./toAPK-bin -v example/VOID-Templates/maelstrom > example/Converted-Apkbuilds/maelstrom

test-ebuild:
	./src/./toAPK-bin -e example/GENTOO-Ebuilds/vis-9999.ebuild > example/Converted-Apkbuilds/vis

test:
	$(MAKE) test-pkgbuild
	$(MAKE) test-template
	$(MAKE) test-ebuild

test-all:
	$(MAKE) test
