# Contributor: Will Sinatra <wpsinatra@gmail.com>
# Maintainer: Will Sinatra <wpsinatra@gmail.com>
pkgname="google-cloud-sdk"
pkgver=291.0.0
pkgrel=0
pkgdesc="A set of command-line tools for the Google Cloud Platform. Includes gcloud (with beta and alpha commands), gsutil, and bq."
url="https://cloud.google.com/sdk/"
license="Apache"
arch="x86_64"
depends="python"
source=
options="!strip staticlibs"

prepare() {
  

  for f in "${source[@]}"; do
    [[ "$f" =~ \.patch$ ]] && \
    ( \
      patch -p1 -i "$srcdir/${f}" > /dev/null 2>&1 ||\
      ( \
        echo "failed to apply patch: $(basename ${f})" && \
        exit 1 \
      ) \
    )
  done
}

package() {
  mkdir "$pkgdir/opt"
  cp -r "$srcdir/google-cloud-sdk" "$pkgdir/opt"

  # The Google code uses a _TraceAction() method which spams the screen even
  # in "quiet" mode, we're throwing away output on purpose to keep it clean
  #  ref: lib/googlecloudsdk/core/platforms_install.py
  python "$pkgdir/opt/google-cloud-sdk/bin/bootstrapping/install.py" \
    --quiet \
    --usage-reporting False \
    --path-update False \
    --bash-completion False \
    --additional-components "" \
    1 > /dev/null

  rm -rf "$pkgdir/opt/google-cloud-sdk/.install/.backup"
  mkdir "$pkgdir/opt/google-cloud-sdk/.install/.backup"
  find $pkgdir -name '__pycache__' -type d -exec rm -rf {} +

  install -D -m 0755 "$srcdir/${source[1]}" \
    "$pkgdir/etc/profile.d/google-cloud-sdk.sh"

  install -D -m 0644 "$pkgdir/opt/google-cloud-sdk/completion.bash.inc" \
    "$pkgdir/etc/bash_completion.d/google-cloud-sdk"

  install -D -m 0644 "$pkgdir/opt/google-cloud-sdk/completion.zsh.inc" \
    "$pkgdir/usr/share/zsh/site-functions/_gcloud"

  mkdir -p "$pkgdir/usr/share"
  mv -f "$pkgdir/opt/google-cloud-sdk/help/man" "$pkgdir/usr/share/"
  chmod 0755 "$pkgdir/usr/share/man"
  chmod 0755 "$pkgdir/usr/share/man/man1"

  mkdir -p "$pkgdir/usr/bin"
  for i in "$pkgdir/opt/google-cloud-sdk/bin"/*; do
    ln -st "$pkgdir/usr/bin/" "${i#$pkgdir}"
  done
  rm -f "$pkgdir"/usr/bin/{bq,dev_appserver.py*,endpointscfg.py*,java_dev_appserver.sh}

  chmod -x "$pkgdir"/usr/share/man/man1/*
  find "$pkgdir/opt/google-cloud-sdk" -name "*.html" -o -name "*.json" -exec chmod -x {} \;
  find "$pkgdir/opt/google-cloud-sdk" -name "*_test.py" -exec chmod +x {} \;
}

