#!/usr/bin/fennel
;; Arch/Void/Gentoo Package to APKBUILD conversion tool
;; Author: Will Sinatra <wpsinatra@gmail.com>
;; GPLv3 | v1.0

;;===== Libraries =====
;;Leaving this here because we may need to use it for http support, but right now we only need https
;;(local http (require "socket.http"))
(local https (require "ssl.https"))
(import-macros {: packagestrip} :macros)

;;===== Globals =====
(global cc { "flastname" "nihil" "email" "<nihil@nihil>" })

(global APKBUILDtbl {"Contributor" (.. "# Contributor: " cc.flastname " " cc.email)
                     "Maintainer" (.. "# Maintainer: " cc.flastname " " cc.email)
                     "PKGN" "pkgname="
                     "PKGV" "pkgver="
                     "PKGR" "pkgrel=0"
                     "PKGD" "pkgdesc="
                     "URL" "url="
                     "ARCH" "arch="
                     "LIC" "license="
                     "SRC" "source="
                     "OPTS" "options="
                     "DEP" "depends="
                     "MDEP" "makedepends="
                     "CDEP" "checkdepends="
                     "BD" "builddir="
                     "REST" "noop"})

;;$_vars go here
(global CUSTOMARGStbl [])

(var dotpath ".toAPK")
;;===== Functions =====
(fn usage [flag]
  (if (= flag nil)
      (print ";;===== toAPK v1.0 =====
Author: Will Sinatra <wpsinatra@gmail.com>
License: GPLv3

;;===== Usage =====
toAPK -a ARCH_PKGBUILD  | Parse Arch Linux PKGBUILD
toAPK -v VOID_TEMPLATE  | Parse VOID Linux Template
toAPK -e GENTOO_EBUILD  | Parse Gentoo Linux Ebuild
toAPK -u -a/e/g PKG     | Pull PKG & Convert it
toAPK -c                | Configure .toAPK
toAPK -h                | Overview, if passed an arg flag, provides further info")
      (or (= flag "-a") (= flag "-v") (= flag "-e"))
      (print ";;===== toAPK -a/-v/-e =====
What?:
Conversion handler for a specific distribution's package build format. Currently Arch Linux PKGBUILDs are best supported. Gentoo Ebuilds, and Void Templates are also supported to a lesser degree of accuracy.

Usage Brief:
toAPK -a /path/to/PKGBUILD
toAPK -v /path/to/Template
toAPK -e /path/to/ebuild

Info:
Parsing occurs line by line, and isn't guarenteed to produce a 100% accurate APKBUILD. Commonly there are errors in the installation functions which need to be correctly be hand. Running the apkbuild-lint tool after converting with toAPK is highly suggested.
")
      (= flag "-c")
      (print ";;===== toAPK -c =====
What?:
Generates the .toAPK configuration file from user input

Usage Brief:
toAPK -c")
      (= flag "-u")
      (print ";;===== toAPK -u =====
What?:
Pulls pkg from distro git repo, and returns converted APKBUILD

Usage Brief:
toAPK -u ?convert_flag ?pkgname ?repo_branch

Usage Examples:
toAPK -u -a fennel aur
toAPK -u -a aspell community
toAPK -u -a htop main
toAPK -u -v angband
toAPK -u -e vis

Info:
Currently the -a Arch flag can be pointed to either a main, community, or AUR repos. Eventually Void and Gentoo packages will support repo branches as well."))
      (os.exit))

;;===== Util =====
(fn directory? [pkg]
  (local f (io.open pkg "r"))
  (if
   (not (f:read 0)) (and (~= (f:seek "end") 0))
   true
   false))

(fn fname [pkg]
  (local name (string.match pkg "^.+/(.+)$"))
  name)

;; Split a string by a delimiter (split "email:<wpsinatra@gmail.com>" ":") -> "email" "<wpsinatra@gmail.com>"
(fn split [val check]
    (if (= check nil)
        (do
         (local check "%s")))
    (local t {})
    (each [str (string.gmatch val (.. "([^" check "]+)"))]
          (do
           (table.insert t str)))
    t)

(fn tailsplit [val check]
  (let [dat (split val check)
        tail (. dat 2)]
    tail))

(fn exists? [file]
  ;;Check if file exists, return boolean
  (local f (io.open file "r"))
  (if
   (= f nil)
   false
   (io.close f)))

;;pull url with ssl backend, write to tmpfile, return tmpf path
(fn getf [url]
  (let
      [(body response) (https.request url)
       tmpf (os.tmpname)]
    (if (= response 400)
        (do
          (print "Bad request encountered")
          (os.exit))
        (= response 404)
        (do
          (print "Resource not found, try a different source")
          (os.exit))
        (= response 301)
        (do
          (print "Resource moved")
          (os.exit))
        (= response 200)
        (do
          (with-open [f (io.open tmpf "w")]
            (f:write body))))
    tmpf))

;;Execute gtf with correct url based on args
(lambda getremotepkg [?typ ?pkgname ?branch]
  (local urltbl {"archaur" (.. "https://aur.archlinux.org/cgit/aur.git/plain/PKGBUILD?h=" ?pkgname)
                 "archcommunity" (.. "https://raw.githubusercontent.com/archlinux/svntogit-community/packages/" ?pkgname "/trunk/PKGBUILD")
                 "archmain" (.. "https://raw.githubusercontent.com/archlinux/svntogit-packages/master/" ?pkgname "/trunk/PKGBUILD")
                 "void"  (.. "https://raw.githubusercontent.com/void-linux/void-packages/master/srcpkgs/" ?pkgname "/template")
                 "gentoo" (.. "https://gitweb.gentoo.org/repo/gentoo.git/plain/" ?pkgname)})
  (if (= ?typ "-a")
      (getf (. urltbl (.. "arch" ?branch)))
      (= ?typ "-e")
      (getf (. urltbl "gentoo"))
      (= ?typ "-v")
      (getf (. urltbl "void"))
      (do
        (print "Unsupported argument was passed")
        (usage))))

;;invoke apkbuild-lint in current directory
;;this doesn't really work without writing the file to an APKBUILD in the current directory and then executing
(fn lint []
  (os.execute "apkbuild-lint"))

;;===== Config Functions =====
(fn setdotpath []
  ;;Set .toAPK path to running users home by concating output of $HOME with /.toAPK
  (set dotpath (.. (os.getenv "HOME") "/.toAPK")))

(fn propogate_conf []
  ;;Build ~/.toAPK from prompt
  (print "Generating .toAPK..")
  (local cf (io.open dotpath "w"))
  (print "Enter your first and last name.")
  (cf:write (.. "flastname:" (io.read)))
  (cf:write "\n")
  (print "Enter your email.")
  (cf:write (.. "email:<" (io.read) ">"))
  (print (.. ".toAPK created at " dotpath))
  (cf:close))

(fn configure []
  ;;If file @ dotpath does not exist, warn, else parse
  (if
   (= (exists? dotpath) false)
   (do
       (io.stderr:write "[WARN]  .toAPK not detected!\n")
       (io.stderr:write "[INFO]  use toAPK -c to configure user variables\n"))
   (do
  ;;For line in .toAPK split @ :
     (each [line (io.lines dotpath)]
       (do
         (var dat (split line ":"))
         (if
          ;; push var data into clientconf (cc) table
          (= (. dat 1) "flastname")
          (do
            (tset cc "flastname" (. dat 2))))
         (if
          (= (. dat 1) "email")
          (do
            (tset cc "email" (. dat 2))))))
     ;;Concat values and push into APKBUILDtbl
     (tset APKBUILDtbl "Contributor" (.. "# Contributor: " cc.flastname " " cc.email ))
     (tset APKBUILDtbl "Maintainer" (.. "# Maintainer: " cc.flastname " " cc.email )))))

;;===== All Package Functions =====
(fn packagerest [pkg]
  ;;check if a function call exists in a package
  ;;'() \\{|()\\{ appears to cause breakage for packages that actually comply
  (local start (assert (io.popen (.. "grep -m 1 -n -E '() \\{' " pkg " | awk -F ':' '{print $1}' "))))
  (local sret (assert (start:read "*a")))
  ;;if grep returns blank set no rest value
  (if (= sret "")
      (do
        (tset APKBUILDtbl "REST" "")))
  ;;else set rest data
  (if (not= sret "")
      (do
        (local total (assert (io.popen (.. "wc -l " pkg " | awk '{print $1}'"))))
        (local tret (assert (total:read "*a")))
        (local funtot (- (tonumber tret) (tonumber sret)))
        (local end (assert (io.popen (.. "grep -A" funtot " -E '() \\{' " pkg))))
        (local eret (assert (end:read "*a")))
        (tset APKBUILDtbl "REST" eret))))

(fn customargs [pkg id]
  (each [line (io.lines pkg)]
    (do
      (local cust (string.match line id))
      (if (not= cust nil)
          (table.insert CUSTOMARGStbl cust)))))

(fn disintercargs []
  (each [k v (pairs CUSTOMARGStbl)]
    (do
      (if (= (string.match v "() {") nil)
          (print v)))))

;;===== Arch PKGBUILD Functions  =====
(fn PKGBUILDclean []
  ;;Clean Arch Pkgbuilds
  (let
      [PKGN (tostring (string.gsub (string.gsub (tailsplit APKBUILDtbl.PKGN "=") "\"" "") "'" ""))]
    (tset APKBUILDtbl "PKGN"
          (-> APKBUILDtbl.PKGN
              (string.lower)))
    (tset APKBUILDtbl "LIC"
          (-> APKBUILDtbl.LIC
              (string.gsub "\"" "")
              (string.gsub "'" "")
              (string.gsub "%(" "\"")
              (string.gsub "%)" "\"")
              (string.gsub "AGPL1" "AGPL-1.0-only")
              (string.gsub "AGPL3" "AGPL-3.0-only")
              (string.gsub "GPL2" "GPL-2.0-only")
              (string.gsub "GPL3" "GPL-3.0-only")))
    (tset APKBUILDtbl "ARCH"
          (-> APKBUILDtbl.ARCH
              (string.gsub "'" "")
              (string.gsub "\"" "")
              (string.gsub "%(" "\"")
              (string.gsub "%)" "\"")
              (string.gsub "i686" "x86")
              (string.gsub "armv7h" "armv7")
              (string.gsub "armv6h" "")
              (string.gsub "arm " "")
              (string.gsub "any" "all")
              (string.gsub "\" " "\"")
              (string.gsub " \"" "\"")))
    (tset APKBUILDtbl "DEP"
          (-> APKBUILDtbl.DEP
              (string.gsub "\"" "")
              (string.gsub "'" "")
              (string.gsub "%(" "\"")
              (string.gsub "%)" "")
              (string.gsub "\"\"" "\"")
              (string.gsub "devel" "dev")
              (string.gsub "python%-" "py3-")
              (.. "\"")))
    (tset APKBUILDtbl "MDEP"
          (-> APKBUILDtbl.MDEP
              (string.gsub "\"" "")
              (string.gsub "'" "")
              (string.gsub "%(" "\"")
              (string.gsub "%)" "")
              (string.gsub "\"\"" "\"")
              (string.gsub "devel" "dev")
              (string.gsub "python%-" "python3-")
              (.. "\"")))
    (tset APKBUILDtbl "CDEP"
          (-> APKBUILDtbl.CDEP
              (string.gsub "\"" "")
              (string.gsub "'" "")
              (string.gsub "%(" "")
              (string.gsub "%)" "")
              (string.gsub "\"\"" "\"")
              (string.gsub "devel" "dev")
              (.. "\"")))
    (tset APKBUILDtbl "OPTS"
          (-> APKBUILDtbl.OPTS
              (string.gsub "%(" "\"")
              (string.gsub "%)" "")
              (string.gsub "'" "")
              (string.gsub "\"\"" "\"")
              (.. "\"")))
    (tset APKBUILDtbl "SRC"
          (-> APKBUILDtbl.SRC
              (string.gsub "%(" "")
              (string.gsub "%)" "")
              (string.gsub "${pkgver}" "$pkgver")
              (string.gsub "${_pkgver}" "$pkgver")
              (string.gsub "$pkgname" PKGN)
              (string.gsub "${pkgname}" PKGN)))
    (tset APKBUILDtbl "REST"
          (-> APKBUILDtbl.REST
              (string.gsub (.. "cd \"$srcdir" "[^\n]*") "")
              (string.gsub (.. "cd \"${srcdir}" "[^\n]*") "")
              (string.gsub "cd %$pkgname%-%$pkgver[^\n]*" "")
              (string.gsub "cd %${pkgname}%-%${pkgver}[^\n]*" "")
              (string.gsub "{LDFLAGS}" "LDFLAGS")
              (string.gsub "{srcdir}" "srcdir")
              (string.gsub "{_pkgver}" "$pkgver")
              (string.gsub "{pkgver}" "pkgver")
              (string.gsub "$pkgname" PKGN)
              (string.gsub "${pkgname}" PKGN)
              (string.gsub "{pkgdir}" "pkgdir")
              (string.gsub "sha256sums.+\n" "")))))
              ;;(string.gsub "(%s%s+)%g+[\n]" "\t"))))) ;;not verbose enough, mangles things


;;===== Void Template Functions =====
(fn TEMPLATEclean [pkg]
  ;;Clean void templates
  (let
      [PKGN (tostring (string.gsub (string.gsub (tailsplit APKBUILDtbl.PKGN "=") "\"" "") "'" ""))]
    (tset APKBUILDtbl "PKGN"
          (-> APKBUILDtbl.PKGN
              (string.lower)))
    (tset APKBUILDtbl "SRC"
          (-> APKBUILDtbl.SRC
              (string.gsub "${homepage}" (string.gsub (tailsplit APKBUILDtbl.URL "=") "\"" ""))
              (string.gsub "//" "/")
              (string.gsub ":/" "://")
              (string.gsub "%${version}" "$pkgver")
              (string.gsub "{version}" "$pkgver")
              (string.gsub "%${pkgname}" PKGN)
              (string.gsub "{" "")
              (string.gsub "}" "")))
    (tset APKBUILDtbl "BD"
          (-> APKBUILDtbl.BD
              (string.gsub "%${version}" "$pkgver")
              (string.gsub "%${pkgname}" "$pkgname")
              (string.gsub "%${revision}" "$pkgrel")
              (string.gsub "%${_" "$_")
              (string.gsub "}" "")))
    (tset APKBUILDtbl "URL"
          (-> APKBUILDtbl.URL
              (string.gsub "${pkgname}" PKGN)))
    (tset APKBUILDtbl "ARCH"
          (-> APKBUILDtbl.ARCH
              (string.gsub "aarch64%*" "aarch64")
              (string.gsub "aarch64-musl" "aarch64")
              (string.gsub "armv7l%*" "armv7")
              (string.gsub "armv7l-musl" "armv7")
              (string.gsub "armv6l%*" "")
              (string.gsub "armv6l-musl" "")
              (string.gsub "i686%*" "x86")
              (string.gsub "i686" "x86")
              (string.gsub "i686-musl" "x86")
              (string.gsub "x86_64%*" "x86_64")
              (string.gsub "x86_64-musl" "x86_64")
              (string.gsub "  " "")))
    (tset APKBUILDtbl "DEP"
          (-> APKBUILDtbl.DEP
              (string.gsub "\"\"" "\"")
              (string.gsub "devel" "dev")))
    (tset APKBUILDtbl "MDEP"
          (-> APKBUILDtbl.MDEP
              (string.gsub "\"\"" "\"")
              (string.gsub "devel" "dev")))
    (tset APKBUILDtbl "CDEP"
          (-> APKBUILDtbl.CDEP
              (string.gsub "\"\"" "\"")
              (string.gsub "devel" "dev")))
    (tset APKBUILDtbl "REST"
          (-> APKBUILDtbl.REST
              (string.gsub "%${pkgname}" PKGN)
              (string.gsub "%${version}" "$pkgver")
              (string.gsub "%${revision}" "$pkgrel")
              (string.gsub "%${DESTDIR}" "$pkgdir")
              (string.gsub "%${FILESDIR}" "$pkgdir")
              (string.gsub "%${wrksrc}" "$builddir")
              (string.gsub "%${sourcepkg}" PKGN)
              (string.gsub "vinstall" "install") ;;install but lazy
              (string.gsub "vmkdir" "mkdir") ;;pkgdir mkdir
              (string.gsub "vmove" "mv") ;;pkgdir mv
              (string.gsub "vcopy" "cp -r") ;;cp -r to pkgdir
              (string.gsub "vlicense" "install -Dm644") ;;/usr/share/licenses install
              (string.gsub "vman" "install -Dm644") ;;/usr/share/man install
              (string.gsub "vdoc" "install -Dm644") ;;/usr/share/doc install
              (string.gsub "vconf" "install -Dm644") ;;/etc/pkgn/conf install
              (string.gsub "vsconf" "install -Dm644") ;;service conf install
              (string.gsub "vsv" "install -Dm644") ;;service install
              (string.gsub "vbin" "install -Dm755"))))) ;;/usr/bin install
  
;;===== Gentoo Ebuild Functions =====
(fn EBUILDpkgnvstrip [pkg]
  (local name (fname pkg))
  (local dat (split name "-"))
  (local nixebuild (split (. dat 2) "."));;yes there's a better way to do this
  (tset APKBUILDtbl "PKGN" (.. APKBUILDtbl.PKGN (. dat 1)))
  (tset APKBUILDtbl "PKGV" (.. APKBUILDtbl.PKGV (. nixebuild 1))))

(fn EBUILDclean []
  ;;Clean Gentoo ebuilds
  (tset APKBUILDtbl "PKGN"
        (-> APKBUILDtbl.PKGN
            (string.lower)))
  (tset APKBUILDtbl "ARCH"
        (-> APKBUILDtbl.ARCH
            (string.gsub "\"" "")))
  (tset APKBUILDtbl "MDEP"
        (-> APKBUILDtbl.MDEP
            (string.gsub "dev%-libs%/" "")))) ;;(%a+) captures, but we'd need a let bind transform, which is likely going to need a custom function, not this big gsub apply

;;===== Debian Deb =====
;; Future Plans

;;===== Redhat RPM =====
;; Future Plans

;;===== Main =====
(fn printAPKBUILD [typ]
  ;;Print APKBUILD from parsed data in APKBUILDtbl
  (print APKBUILDtbl.Contributor)
  (print APKBUILDtbl.Maintainer)
  (print APKBUILDtbl.PKGN)
  (print APKBUILDtbl.PKGV)
  (print APKBUILDtbl.PKGR)
  (print APKBUILDtbl.PKGD)
  (print APKBUILDtbl.URL)
  (if (= typ "-v")
      ;;If VOID do specifics
      (do
        (if (not= APKBUILDtbl.ARCH "arch=")
            (print APKBUILDtbl.ARCH))
        (if (= APKBUILDtbl.ARCH "arch=")
            (print (.. APKBUILDtbl.ARCH "\"x86_64\"")))))
  (print APKBUILDtbl.LIC)
  (if (= typ "-a")
      ;;If ARCH do specifics
      (do
        (print APKBUILDtbl.ARCH)
        (if (not= APKBUILDtbl.MDEP "makedepends=\"")
            (print APKBUILDtbl.MDEP))
        (if (not= APKBUILDtbl.CDEP "checkdepends=\"")
            (print APKBUILDtbl.CDEP))
        (if (not= APKBUILDtbl.DEP "depends=\"")
            (print APKBUILDtbl.DEP))))
  ;;If void do specifics
  (if (= typ "-v")
      (do
        (if (not= APKBUILDtbl.MDEP "makedepends=")
            (print APKBUILDtbl.MDEP))
        (if (not= APKBUILDtbl.CDEP "checkdepends=")
            (print APKBUILDtbl.CDEP))
        (if (not= APKBUILDtbl.DEP "depends=")
            (print APKBUILDtbl.DEP))))
  ;;If Gentoo do specifics
  (if (= typ "-e")
      (do
        (if (not= APKBUILDtbl.ARCH "arch=")
            (print APKBUILDtbl.ARCH))
        (if (= APKBUILDtbl.ARCH "arch=")
            (print (.. APKBUILDtbl.ARCH "\"x86_64\"")))
        (if (not= APKBUILDtbl.MDEP "makedepends=")
            (print APKBUILDtbl.MDEP))
        (if (not= APKBUILDtbl.CDEP "checkdepends=")
            (print APKBUILDtbl.CDEP))
        (if (not= APKBUILDtbl.DEP "depends=")
            (print APKBUILDtbl.DEP))))
  (if (> (# CUSTOMARGStbl) 0)
      (disintercargs))
  (print APKBUILDtbl.SRC)
  (if (not= APKBUILDtbl.OPTS "options=\"")
      (print APKBUILDtbl.OPTS))
  (if (not= APKBUILDtbl.BD "builddir=")
      (print APKBUILDtbl.BD))
  (print "")
  (if (not= APKBUILDtbl.REST "")
      (print APKBUILDtbl.REST)))
  
;; toAPK -a ARCH_PKGBUILD
;; toAPK -v VOID_TEMPLATE
;; toAPK -e GENTOO_EBUILD
(lambda toAPK [?argv1 ?argv2 ?argv3 ?argv4]
  ;;Main function
  (if
   (or (= ?argv1 "-h") (= ?argv1 "") (= ?argv1 "--help") (= ?argv1 nil))
   (usage ?argv2))
   (if (= ?argv1 "-c")
       (propogate_conf dotpath))
   (if (= ?argv1 "-a")
       (do
         (configure)
         (packagestrip ?argv2 {"pkgname" "PKGN"
                              "pkgver" "PKGV"
                              "pkgdesc" "PKGD"
                              "url" "URL"
                              "arch" "ARCH"
                              "license" "LIC"
                              "source" "SRC"
                              "makedepends" "MDEP"
                              "checkdepends" "CDEP"
                              "depends" "DEP"
                              "options" "OPTS"})
         (customargs ?argv2 "^_.*")
         (packagerest ?argv2)
         (PKGBUILDclean)
         (printAPKBUILD ?argv1)))
   (if (= ?argv1 "-e")
       (do
         (configure)
         (EBUILDpkgnvstrip ?argv2)
         (packagestrip ?argv2 {"DESCRIPTION" "PKGD"
                              "HOMEPAGE" "URL"
                              "KEYWORDS" "ARCH"
                              "LICENSE" "LIC"
                              "SRC_URI" "SRC"
                              "EGIT_REPO_URI" "SRC"
                              "RDEPEND" "DEP"
                              "DEPEND" "MDEP"})
         (customargs ?argv2 "^_.*")
         (packagerest ?argv2)
         (EBUILDclean)
         (printAPKBUILD ?argv1)))
   (if (= ?argv1 "-v")
       (do
         (if (= (directory? ?argv2) false)
             (do
               (configure)
               (packagestrip ?argv2 {"pkgname" "PKGN"
                                    "version" "PKGV"
                                    "short_desc" "PKGD"
                                    "homepage" "URL"
                                    "license" "LIC"
                                    "distfiles" "SRC"
                                    "checkdepends" "CDEP"
                                    "makedepends" "MDEP"
                                    "depends" "DEP"
                                    "only_for_archs" "ARCH"
                                    "archs" "ARCH"
                                    "wrksrc" "BD"})
               (customargs ?argv2 "^_.*")
               (packagerest ?argv2)
               (TEMPLATEclean)
               (printAPKBUILD ?argv1)))
         (if (= (directory? ?argv2) true)
             (print "VOID Template directory parsing is currently unsupported.\nSupport is planned for a later release, try running toAPK on the template file directly for now."))))
   (if (= ?argv1 "-u")
       (do
         (let
             [flag ?argv2
              tmpf (getremotepkg ?argv2 ?argv3 ?argv4)]
           (toAPK flag tmpf)))))
  
;;===== Run Main =====
(setdotpath)
(toAPK ...)
