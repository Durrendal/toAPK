(fn packagestrip [pkg checks]
  `(let
       [split# (fn split# [val# check#]
                 (if (= check# nil)
                     (do
                       (local check# "%s")))
                 (local t# {})
                 (each [str# (string.gmatch val# (.. "([^" check# "]+)"))]
                   (do
                     (table.insert t# str#)))
                 t#)]
     (each [line# (io.lines ,pkg)]
       (do
         (var dat# (split# line# "="))
         (each [k# v# (pairs ,checks)]
           (do
             (if
              (= (. dat# 1) k#)
              (tset APKBUILDtbl v# (.. (. APKBUILDtbl v#) (. dat# 2))))))))))

{:packagestrip packagestrip}
